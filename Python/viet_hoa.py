#viết hoa chử cái đầu
a ='đoàn cao vỉ'
b = a.capitalize()

print(b)
#viết hoa tất cả ký tự
a ='đoàn cao vỉ'
b = a.upper()

print(b)
#viết thường hết tất cả các ký tự
a ='đoàn cao vỉ'
b = a.lower()

print(b)

#chuyển thường thành hoa và ngược lại
a ='đoàn cao vỉ'
b = a.swapcase()

print(b)

#viết hoa đầu mỗi từ
a ='đoàn cao vỉ'
b = a.title()

print(b)

#căn giữa số đơn vị cho trước center(số đơn vị,'ký tự hoặc khoảng trắng')
a ='đoàn cao vỉ'
b = a.center(30, '-' )

print(b)

#căn phải số đơn vị cho trước rjust(số đơn vị,'ký tự hoặc khoảng trắng')
a ='đoàn cao vỉ'
b = a.rjust(30, '-' )

print(b)

#căn trái số đơn vị cho trước ljust(số đơn vị,'ký tự hoặc khoảng trắng')
a ='đoàn cao vỉ'
b = a.ljust(30, '-' )

print(b)

#mã hoá ký tự
a ='đoàn'
b = a.encode(encoding='utf-8', errors='strict')
print(a)
print(b)

#cộng chuổi theo thứ tự join(['','','',...])
a = 'là số'
b = a.join(['1\t','\t2'])
print(b)

#thay thế chuổi theo thứ tự replace('ký tự thay thế','ký tự cần thay thế',...)
a = 'là số số'
b = a.replace('số', 'chữ')
print(b)

#cắt ký tự hoặc khoảng trắng ở hai đầu strip('ký tự cần cắt')
a = '     vvĩvvvvv                '
b = a.strip()
d = 'ddddvĩdddddddddd'
c = d.strip('d')
print(b)
print(c)