#cắt chuổi từ ký tự chỉ định split('ký tự cần cắt')
a = 'đoàn cao vỉoo'
b = a.split('o')
print(b)
#cắt chuổi từ ký tự chỉ định partition('ký tự cần cắt')
a = 'đoàn cao vỉoo'
b = a.partition('o')
print(b)


#đếm ký tự trong chuổi count('ký tự cần đếm')
a = 'đoàn cao vỉoo'
b = a.count('o')
print(b)

#tìm vị trí index từ ký tự chỉ định trong chuổi find('ký tự cần tìm')
a = 'đoàn cao vỉoo'
b = a.find('cao')
print(b)

#tìm vị trí index từ ký tự chỉ định trong chuổi như find nhưng tìm không thấy sẽ in ra lỗi index('ký tự cần tìm')
a = 'đoàn cao vỉoo'
b = a.index('c')
print(b)

# is... kiểm tra xem nó có phải là kiểu nó ko nếu phải trả về true ngược lại là false
a = 'đoàn cao vỉ'
b = a.islower()#islower() kiểm tra xem chuổi a có phải là viết thường không
print(b)
#tương tự như trên ta cũng có: isupper(), isspace()--> có khoảng trắng ko.